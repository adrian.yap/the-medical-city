﻿using TMC.CVLEmailing.Processes;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var str1 = "2D Echo With Doppler Study (Color Flow) - Adult";
            var str2 = "2D Echo With Doppler Study (Color Flow) - Adult";
            // Console.WriteLine((int)str1[2]);
            // Console.WriteLine((int)str2[2]);
            Console.WriteLine(Regex.Split(str1, @"\s").Intersect(Regex.Split(str2, @"\s")).Count());
            // Console.WriteLine($" { string.Join(Environment.NewLine, Regex.Split(str1, @"\s")) } \n\n{ string.Join(Environment.NewLine, Regex.Split(str2, @"\s")) }");
            // Console.WriteLine($"{ string.Join(Environment.NewLine, str1.Split(" ").Except(str2.Split(" "))) }");
        }
    }
}