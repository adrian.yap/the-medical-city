using System.Activities.Presentation.Metadata;
using System.ComponentModel;
using System.ComponentModel.Design;
using TMC.CVLEmailing.Activities.Design.Designers;
using TMC.CVLEmailing.Activities.Design.Properties;

namespace TMC.CVLEmailing.Activities.Design
{
    public class DesignerMetadata : IRegisterMetadata
    {
        public void Register()
        {
            var builder = new AttributeTableBuilder();
            builder.ValidateTable();

            var categoryAttribute = new CategoryAttribute($"{Resources.Category}");

            builder.AddCustomAttributes(typeof(CreateCVLEmailingDatabase), categoryAttribute);
            builder.AddCustomAttributes(typeof(CreateCVLEmailingDatabase), new DesignerAttribute(typeof(CreateCVLEmailingDatabaseDesigner)));
            builder.AddCustomAttributes(typeof(CreateCVLEmailingDatabase), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(GetResults), categoryAttribute);
            builder.AddCustomAttributes(typeof(GetResults), new DesignerAttribute(typeof(GetResultsDesigner)));
            builder.AddCustomAttributes(typeof(GetResults), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(GetPatientDetails), categoryAttribute);
            builder.AddCustomAttributes(typeof(GetPatientDetails), new DesignerAttribute(typeof(GetPatientDetailsDesigner)));
            builder.AddCustomAttributes(typeof(GetPatientDetails), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(GetAttachments), categoryAttribute);
            builder.AddCustomAttributes(typeof(GetAttachments), new DesignerAttribute(typeof(GetAttachmentsDesigner)));
            builder.AddCustomAttributes(typeof(GetAttachments), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(AccessDatabase), categoryAttribute);
            builder.AddCustomAttributes(typeof(AccessDatabase), new DesignerAttribute(typeof(AccessDatabaseDesigner)));
            builder.AddCustomAttributes(typeof(AccessDatabase), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(Report), categoryAttribute);
            builder.AddCustomAttributes(typeof(Report), new DesignerAttribute(typeof(ReportDesigner)));
            builder.AddCustomAttributes(typeof(Report), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(GetEmailStatusData), categoryAttribute);
            builder.AddCustomAttributes(typeof(GetEmailStatusData), new DesignerAttribute(typeof(GetEmailStatusDataDesigner)));
            builder.AddCustomAttributes(typeof(GetEmailStatusData), new HelpKeywordAttribute(""));


            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
    }
}