using System;
using System.Activities;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Add entry to reports database.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.Report_DisplayName))]
    [LocalizedDescription(nameof(Resources.Report_Description))]
    public class Report : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Database file.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.DBFile_DisplayName))]
        [LocalizedDescription(nameof(Resources.DBFile_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> DBFile { get; set; }

        /// <summary>
        /// Patient Details (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.PatientDetails_DisplayName))]
        [LocalizedDescription(nameof(Resources.PatientDetails_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<PatientDetails> PatientDetails { get; set; }

        /// <summary>
        /// Procedure (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Procedure_DisplayName))]
        [LocalizedDescription(nameof(Resources.Procedure_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<Procedure> Procedure { get; set; }

        /// <summary>
        /// Select report type.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Report_ReportType_DisplayName))]
        [LocalizedDescription(nameof(Resources.Report_ReportType_Description))]
        [LocalizedCategory(nameof(Resources.Options_Category))]
        public ReportType ReportType { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Report()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (DBFile == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(DBFile)));
            if (PatientDetails == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(PatientDetails)));
            if (Procedure == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(Procedure)));

            base.CacheMetadata(metadata);
        }

        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var _DBFile = DBFile.Get(context);
            var patientDetails = PatientDetails.Get(context);
            var procedure = Procedure.Get(context);

            // Process

            Reporting.AddEntry(ReportType, _DBFile, patientDetails, procedure);

            // Outputs

            return (ctx) => {
            };
        }

        #endregion
    }
}