using System;
using System.Activities;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Get patient details.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.GetPatientDetails_DisplayName))]
    [LocalizedDescription(nameof(Resources.GetPatientDetails_Description))]
    public class GetPatientDetails : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Text extracted from the SOA file.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetPatientDetails_SOAStr_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetPatientDetails_SOAStr_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> SOAStr { get; set; }

        /// <summary>
        /// Dictionary(Of String, DataTable) that contains datatables.
        /// * Key(String) - Given name of DataTable.
        /// * Value(DataTable) - Corresponding DataTable.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetPatientDetails_DataTablesDictionary_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetPatientDetails_DataTablesDictionary_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<Dictionary<string, DataTable>> DataTablesDictionary { get; set; }

        /// <summary>
        /// Patient details. (Custom class)
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.PatientDetails_DisplayName))]
        [LocalizedDescription(nameof(Resources.PatientDetails_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<PatientDetails> PatientDetails { get; set; }

        /// <summary>
        /// Select logging condition.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetPatientDetails_LoggingOption_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetPatientDetails_LoggingOption_Description))]
        [LocalizedCategory(nameof(Resources.Options_Category))]
        public LoggingOption LoggingOption { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetPatientDetails()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (SOAStr == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(SOAStr)));
            if (DataTablesDictionary == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(DataTablesDictionary)));

            base.CacheMetadata(metadata);
        }

        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var SOAstr = SOAStr.Get(context);
            var datatablesDictionary = DataTablesDictionary.Get(context);

            // Process

            var patientDetails = PatientDetailsGathering.GetPatientDetails(SOAstr, datatablesDictionary, LoggingOption);

            // Outputs

            return (ctx) => {
                PatientDetails.Set(ctx, patientDetails);
            };
        }

        #endregion
    }
}