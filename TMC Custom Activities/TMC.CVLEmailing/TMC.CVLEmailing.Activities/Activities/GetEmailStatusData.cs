using System;
using System.Activities;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Get Email Status data.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.GetEmailStatusData_DisplayName))]
    [LocalizedDescription(nameof(Resources.GetEmailStatusData_Description))]
    public class GetEmailStatusData : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Database file.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.DBFile_DisplayName))]
        [LocalizedDescription(nameof(Resources.DBFile_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> DBFile { get; set; }

        /// <summary>
        /// Dictionary(Of String, DataTable) that contains email status data.
        ///     Key(String) - Report Type (Pending, Failed, Not Sent, Sent).
        ///     Value(DataTable) - Data of corresponding report.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Data_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetEmailStatusData_Data_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<Dictionary<string, DataTable>> Data { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetEmailStatusData()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (DBFile == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(DBFile)));

            base.CacheMetadata(metadata);
        }

        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var _DBFile = DBFile.Get(context);

            // Process

            var data = Reporting.GetEmailStatusData(_DBFile);

            // Outputs

            return (ctx) => {
                Data.Set(ctx, data);
            };
        }

        #endregion
    }
}