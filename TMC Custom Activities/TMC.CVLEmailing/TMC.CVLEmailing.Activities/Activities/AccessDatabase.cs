using System;
using System.Activities;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Access database.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.AccessDatabase_DisplayName))]
    [LocalizedDescription(nameof(Resources.AccessDatabase_Description))]
    public class AccessDatabase : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Database file.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.DBFile_DisplayName))]
        [LocalizedDescription(nameof(Resources.DBFile_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> DBFile { get; set; }

        /// <summary>
        /// Valid SQLite command.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.AccessDatabase_SQLiteCommand_DisplayName))]
        [LocalizedDescription(nameof(Resources.AccessDatabase_SQLiteCommand_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> SQLiteCommand { get; set; }

        /// <summary>
        /// Data returned.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Data_DisplayName))]
        [LocalizedDescription(nameof(Resources.Data_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<DataTable> Data { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AccessDatabase()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (DBFile == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(DBFile)));
            if (SQLiteCommand == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(SQLiteCommand)));

            base.CacheMetadata(metadata);
        }


        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var _DBFile = DBFile.Get(context);
            var _SQLiteCommand = SQLiteCommand.Get(context);

            // Process

            var data = Database.AccessDatabase(_DBFile, _SQLiteCommand);

            // Outputs

            return (ctx) => {
                Data.Set(ctx, data);
            };
        }

        #endregion
    }
}