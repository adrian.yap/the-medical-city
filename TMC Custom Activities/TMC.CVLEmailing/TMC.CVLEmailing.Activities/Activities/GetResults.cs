using System;
using System.Activities;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Models;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Get available results in results directories.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.GetResults_DisplayName))]
    [LocalizedDescription(nameof(Resources.GetResults_Description))]
    public class GetResults : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Process config.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetResults_Config_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetResults_Config_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<Dictionary<string, object>> Config { get; set; }

        /// <summary>
        /// Results (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Results_DisplayName))]
        [LocalizedDescription(nameof(Resources.Results_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<Results> Results { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetResults()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (Config == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(Config)));

            base.CacheMetadata(metadata);
        }

        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var config = Config.Get(context);

            // Process

            var results = ResultsGathering.GetResults(config);

            // Outputs
            return (ctx) => {
                Results.Set(ctx, results);
            };
        }

        #endregion
    }
}