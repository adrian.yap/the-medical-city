using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TMC.CVLEmailing.Activities.Properties;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;
using TMC.CVLEmailing.Processes;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;

namespace TMC.CVLEmailing.Activities
{
    /// <summary>
    /// Get attachments.
    /// </summary>
    [LocalizedDisplayName(nameof(Resources.GetAttachments_DisplayName))]
    [LocalizedDescription(nameof(Resources.GetAttachments_Description))]
    public class GetAttachments : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        /// <summary>
        /// Results (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Results_DisplayName))]
        [LocalizedDescription(nameof(Resources.Results_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<Results> Results { get; set; }

        /// <summary>
        /// Procedure (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.Procedure_DisplayName))]
        [LocalizedDescription(nameof(Resources.Procedure_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<Procedure> Procedure { get; set; }

        /// <summary>
        /// Patient Details (Custom class).
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.PatientDetails_DisplayName))]
        [LocalizedDescription(nameof(Resources.PatientDetails_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<PatientDetails> PatientDetails { get; set; }

        /// <summary>
        /// Database file.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.DBFile_DisplayName))]
        [LocalizedDescription(nameof(Resources.DBFile_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> DBFile { get; set; }

        /// <summary>
        /// Attachments gathered.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetAttachments_Attachments_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetAttachments_Attachments_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<string[]> Attachments { get; set; }

        /// <summary>
        /// Select procedure group.
        /// </summary>
        [LocalizedDisplayName(nameof(Resources.GetAttachments_ProcedureGroup_DisplayName))]
        [LocalizedDescription(nameof(Resources.GetAttachments_ProcedureGroup_Description))]
        [LocalizedCategory(nameof(Resources.Options_Category))]
        public ProcedureGroup ProcedureGroup { get; set; }

        #endregion


        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GetAttachments()
        {
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Cache metadata.
        /// </summary>
        /// <param name="metadata"></param>
        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (Results == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(Results)));
            if (Procedure == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(Procedure)));
            if (PatientDetails == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(PatientDetails)));
            if (DBFile == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(DBFile)));

            base.CacheMetadata(metadata);
        }

        /// <summary>
        /// Execute async.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs

            var results = Results.Get(context);
            var procedure = Procedure.Get(context);
            var patientDetails = PatientDetails.Get(context);
            var _DBFile = DBFile.Get(context);
            var attachments= new List<string>();

            // Process

            attachments = AttachmentsGathering.GetAttachments(results, ProcedureGroup, procedure, patientDetails, _DBFile).ToList();

            // Outputs

            return (ctx) => {
                Attachments.Set(ctx, attachments.ToArray());
            };
        }

        #endregion
    }
}