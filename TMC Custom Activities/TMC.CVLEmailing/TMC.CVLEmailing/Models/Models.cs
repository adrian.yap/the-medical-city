﻿using System;
using System.Collections.Generic;

namespace TMC.CVLEmailing.Models
{
    /// <summary>
    /// Results (Custom class).
    /// </summary>
    public class Results
    {
        #region Fields

        private string[] _vascularResults;
        private string[] _tiltResults;
        private string[] _echoResults;
        private string[] _holterResults;
        private string[] _ABPMResults;
        private string[] _stressTestResults;
        private string[] _ECGResults;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Results()
        {
        }

        /// <summary>
        /// Overload #01.
        /// </summary>
        /// <param name="vascularResults"></param>
        /// <param name="tiltResults"></param>
        /// <param name="echoResults"></param>
        /// <param name="holterResults"></param>
        /// <param name="ABPMResults"></param>
        /// <param name="stressTestResults"></param>
        /// <param name="ECGResults"></param>
        public Results(string[] vascularResults, string[] tiltResults, string[] echoResults, string[] holterResults, string[] ABPMResults, string[] stressTestResults, string[] ECGResults)
        {
            _vascularResults = vascularResults;
            _tiltResults = tiltResults;
            _echoResults = echoResults;
            _holterResults = holterResults;
            _ABPMResults = ABPMResults;
            _stressTestResults = stressTestResults;
            _ECGResults = ECGResults;
        }


        #endregion

        #region Properties

        /// <summary>
        /// Vascular results.
        /// </summary>
        public string[] VascularResults { get { return _vascularResults; } }

        /// <summary>
        /// Tilt results.
        /// </summary>
        public string[] TiltResults { get { return _tiltResults; } }

        /// <summary>
        /// Echo results.
        /// </summary>
        public string[] EchoResults { get { return _echoResults; } }

        /// <summary>
        /// Holter results.
        /// </summary>
        public string[] HolterResults { get { return _holterResults; } }

        /// <summary>
        /// ABPM results.
        /// </summary>
        public string[] ABPMResults { get { return _ABPMResults; } }

        /// <summary>
        /// Stress Test results.
        /// </summary>
        public string[] StressTestResults { get { return _stressTestResults; } }

        /// <summary>
        /// ECG results.
        /// </summary>
        public string[] ECGResults { get { return _ECGResults; } }

        #endregion
    }

    /// <summary>
    /// Procedure (Custom class).
    /// </summary>
    public class Procedure : IEquatable<Procedure>
    {
        #region Fields

        private string _itemCode, _procedureDate, _procedureName, _procedureGroup, _procedureCode;
        private List<string> _attachments = new List<string>();

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Procedure()
        {
        }

        /// <summary>
        /// Overload #01.
        /// </summary>
        /// <param name="itemCode"></param>
        /// <param name="procedureDate"></param>
        /// <param name="procedureName"></param>
        /// <param name="procedureGroup"></param>
        /// <param name="procedureCode"></param>
        public Procedure(string itemCode, string procedureDate, string procedureName, string procedureGroup, string procedureCode)
        {
            _itemCode = itemCode;
            _procedureDate = procedureDate;
            _procedureName = procedureName;
            _procedureGroup = procedureGroup;
            _procedureCode = procedureCode;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Item code from SOA file.
        /// </summary>
        public string ItemCode { get { return _itemCode; } }

        /// <summary>
        /// Procedure date from SOA file.
        /// </summary>
        public string ProcedureDate { get { return _procedureDate; } }

        /// <summary>
        /// Procedure name mapped from item code map.
        /// </summary>
        public string ProcedureName { get { return _procedureName; } }

        /// <summary>
        /// Procedure group mapped from CVL procedure map.
        /// </summary>
        public string ProcedureGroup { get { return _procedureGroup; } }

        /// <summary>
        /// Procedure code mapped from procedure code map.
        /// </summary>
        public string ProcedureCode { get { return _procedureCode; } }

        /// <summary>
        /// Attachments of given procedure.
        /// </summary>
        public List<string> Attachments { get { return _attachments; } }

        #endregion

        #region Methods

        /// <summary>
        /// Add attachment.
        /// </summary>
        /// <param name="attachment"></param>
        public void AddAttachment(string attachment)
        {
            _attachments.Add(attachment);
        }

        /// <summary>
        /// "Equals" method implementation.
        /// </summary>
        /// <param name="procedure"></param>
        /// <returns></returns>
        public bool Equals(Procedure procedure)
        {
            if (procedure is null) return false;

            return _itemCode == procedure.ItemCode &&
                   _procedureDate == procedure.ProcedureDate &&
                   _procedureName == procedure.ProcedureName &&
                   _procedureGroup == procedure.ProcedureGroup &&
                   _procedureCode == procedure.ProcedureCode;
        }

        /// <summary>
        /// "Equals" method override.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) => Equals(obj as Procedure);

        /// <summary>
        /// "GetHashCode" method override.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => _itemCode.GetHashCode() ^ 
                                             _procedureDate.GetHashCode() ^
                                             _procedureName.GetHashCode() ^
                                             _procedureGroup.GetHashCode() ^
                                             _procedureCode.GetHashCode();

        #endregion
    }

    /// <summary>
    /// Patient Details (Custom class).
    /// </summary>
    public class PatientDetails
    {
        #region Fields

        private string _SOANumber, _orderNumber, _PIN, _firstName, _middleName, _lastName, _suffix, _fullName, _dateOfBirth;
        private Procedure[] _procedures;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PatientDetails()
        {
        }

        /// <summary>
        /// Overload #01.
        /// </summary>
        /// <param name="SOANumber"></param>
        /// <param name="orderNumber"></param>
        /// <param name="PIN"></param>
        /// <param name="firstName"></param>
        /// <param name="middleName"></param>
        /// <param name="lastName"></param>
        /// <param name="suffix"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="procedures"></param>
        public PatientDetails(string SOANumber, string orderNumber, string PIN, string firstName, string middleName, string lastName, string suffix, string dateOfBirth, Procedure[] procedures)
        {
            _SOANumber = SOANumber;
            _orderNumber = orderNumber;
            _PIN = PIN;
            _firstName = firstName;
            _middleName = middleName;
            _lastName = lastName;
            _suffix = suffix;
            _fullName = $"{ lastName }, { firstName } { middleName } { suffix }".Trim();
            _dateOfBirth = dateOfBirth;
            _procedures = procedures;
        }

        #endregion

        #region Properties

        /// <summary>
        /// SOA number (SOA No.) from SOA file.
        /// </summary>
        public string SOANumber { get { return _SOANumber; } }

        /// <summary>
        /// Order number queried from Orion database.
        /// </summary>
        public string OrderNumber { get { return _orderNumber; } }

        /// <summary>
        /// PIN (HN) from SOA file.
        /// </summary>
        public string PIN { get { return _PIN; } }

        /// <summary>
        /// Patient first name queried from Orion database.
        /// </summary>
        public string FirstName { get { return _firstName; } }

        /// <summary>
        /// Patient middle name queried from Orion database.
        /// </summary>
        public string MiddleName { get { return _middleName; } }

        /// <summary>
        /// Patient last name queried from Orion database.
        /// </summary>
        public string LastName { get { return _lastName; } }

        /// <summary>
        /// Patient suffix queried from Orion database.
        /// </summary>
        public string Suffix { get { return _suffix; } }

        /// <summary>
        /// Patient full name ( {Last Name}, {First Name} {Middle Name} {Suffix} )
        /// </summary>
        public string FullName { get { return _fullName; } }

        /// <summary>
        /// Patient date of birth queried from Orion database.
        /// </summary>
        public string DateOfBirth { get { return _dateOfBirth; } }

        /// <summary>
        /// Patient CVL procedures.
        /// </summary>
        public Procedure[] Procedures { get { return _procedures; } }

        #endregion
    }
}