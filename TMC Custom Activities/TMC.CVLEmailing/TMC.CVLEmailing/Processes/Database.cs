﻿using System;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace TMC.CVLEmailing.Processes
{
    /// <summary>
    /// 
    /// Custom class with methods for creating and accessing process database.
    /// 
    /// Used in the following activities:
    /// - Access Database
    /// - Create CVL Emailing Database
    /// 
    /// Used in the following processes:
    /// - Attachments Gathering
    /// - Reporting
    /// 
    /// </summary>

    public class Database
    {
        /// <summary>
        /// Create monitoring database.
        /// </summary>
        /// <param name="DBFile"></param>
        public static void CreateCVLEmailingDatabase(string DBFile)
        {
            try
            {
                SQLiteConnection.CreateFile(DBFile);

                using (var SQLiteConnection = new SQLiteConnection($"Data Source = { DBFile }"))
                {
                    SQLiteConnection.Open();
                    var SQLiteCommandSTR = $"CREATE TABLE \"Sent\" ( \"pin_number\" TEXT NOT NULL, \"patient_name\" TEXT NOT NULL, \"procedure_name\" TEXT NOT NULL, \"procedure_date\" TEXT NOT NULL, \"date_and_time_sent\" TEXT NOT NULL );" +
                                           $"CREATE TABLE \"NotSent\" ( \"pin_number\" TEXT NOT NULL, \"patient_name\" TEXT NOT NULL, \"procedure_name\" TEXT NOT NULL, \"procedure_date\" TEXT NOT NULL, \"date_and_time_sent\" TEXT NOT NULL );" +
                                           $"CREATE TABLE \"Pending\" ( \"pin_number\" TEXT NOT NULL, \"patient_name\" TEXT NOT NULL, \"procedure_name\" TEXT NOT NULL, \"procedure_date\" TEXT NOT NULL, \"date_and_time_sent\" TEXT NOT NULL );" +
                                           $"CREATE TABLE \"Failed\" ( \"pin_number\" TEXT NOT NULL, \"patient_name\" TEXT NOT NULL, \"procedure_name\" TEXT NOT NULL, \"procedure_date\" TEXT NOT NULL, \"date_and_time_sent\" TEXT NOT NULL );" +
                                           $"CREATE TABLE \"Exceptions\" ( \"date_and_time\" TEXT NOT NULL, \"process_stage\" TEXT NOT NULL, \"source\" TEXT NOT NULL, \"message\" TEXT NOT NULL, \"full_exception\" TEXT NOT NULL );" +
                                           $"CREATE INDEX sent_index ON Sent (pin_number, procedure_name);" +
                                           $"CREATE INDEX notSent_index ON NotSent (pin_number, procedure_name);" +
                                           $"CREATE INDEX pending_index ON Pending (pin_number, procedure_name);" +
                                           $"CREATE INDEX failed_index ON Failed (pin_number, procedure_name);";

                    var SQLiteCommand = new SQLiteCommand(SQLiteCommandSTR, SQLiteConnection);
                    SQLiteCommand.ExecuteNonQuery();
                    SQLiteConnection.Close();
                    File.SetAttributes(DBFile, FileAttributes.Hidden);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("CreateCVLEmailingDatabase error", exception);
            }
        }

        /// <summary>
        /// Access database.
        /// </summary>
        /// <param name="DBFile"></param>
        /// <param name="SQLiteCommand"></param>
        /// <returns></returns>
        public static DataTable AccessDatabase(string DBFile, string SQLiteCommand)
        {
            try
            {
                using (var SQLiteConnection = new SQLiteConnection($"Data Source = { DBFile }"))
                {
                    using (var SQLiteDataAdapter = new SQLiteDataAdapter(SQLiteCommand, SQLiteConnection))
                    {
                        var dataTable = new DataTable();
                        SQLiteDataAdapter.Fill(dataTable);

                        return dataTable;
                    }
                }
            }
            catch (Exception exception)
            {
                throw new Exception("AccessDatabase error", exception);
            }
        }
    }
}