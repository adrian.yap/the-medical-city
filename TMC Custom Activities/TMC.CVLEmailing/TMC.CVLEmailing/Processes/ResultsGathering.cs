﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMC.CVLEmailing.Models;

namespace TMC.CVLEmailing.Processes
{
    /// <summary>
    /// 
    /// Custom class with methods for gathering results.
    /// 
    /// Used in the following activities:
    /// - Results Gathering.
    /// 
    /// </summary>

    public class ResultsGathering
    {
        /// <summary>
        /// Get results.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static Results GetResults(Dictionary<string, object> config)
        {
            try
            {
                #region Initialize variables

                var vascularResults = new List<string>();
                var tiltResults = new List<string>();
                var echoResults = new List<string>();
                var holterResults = new List<string>();
                var ABPMResults = new List<string>();
                var stressTestResults = new List<string>();
                var folderMap = new Dictionary<string, KeyValuePair<string, string>>()
                {
                    { "Vascular Results", new KeyValuePair<string, string>(config["vascular_results_dir"].ToString().Trim(), config["vascular_dir_search_pattern"].ToString().Trim()) },
                    { "Tilt Results", new KeyValuePair<string, string>(config["tilt_results_dir"].ToString().Trim(), config["tilt_dir_search_pattern"].ToString().Trim()) },
                    { "Echo Results", new KeyValuePair<string, string>(config["echo_results_dir"].ToString().Trim(), config["echo_dir_search_pattern"].ToString().Trim()) },
                    { "Holter Results", new KeyValuePair<string, string>(config["holter_results_dir"].ToString().Trim(), config["holter_dir_search_pattern"].ToString().Trim()) },
                    { "ABPM Results", new KeyValuePair<string, string>(config["ABPM_results_dir"].ToString().Trim(), config["ABPM_dir_search_pattern"].ToString().Trim()) },
                    { "Stress Test Results", new KeyValuePair<string, string>(config["stress_test_results_dir"].ToString().Trim(), config["stress_test_dir_search_pattern"].ToString().Trim()) },
                    { "ECG Results", new KeyValuePair<string, string>(config["renamed_ECG_results_dir"].ToString().Trim(), resultFilePattern) }
                };

                #endregion

                #region Gather data

                #region Vascular results

                var vascularDirectories = Directory.GetDirectories(folderMap["Vascular Results"].Key, folderMap["Vascular Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting Vascular results in { folderMap["Vascular Results"].Key }");

                foreach (var vascularDirectory in vascularDirectories)
                {
                    foreach (var _file in GetFiles(vascularDirectory, resultFilePattern))
                    {
                        vascularResults.Add(_file);
                    }
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Vascular results: { string.Format("{0:n0}", vascularResults.Count()) }");

                #endregion

                #region Tilt results

                var tiltDirectories = Directory.GetDirectories(folderMap["Tilt Results"].Key, folderMap["Tilt Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting Tilt results in { folderMap["Tilt Results"].Key }");

                foreach (var tiltDirectory in tiltDirectories)
                {
                    foreach (var _file in GetFiles(tiltDirectory, resultFilePattern))
                    {
                        tiltResults.Add(_file);
                    }
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Tilt results: { string.Format("{0:n0}", tiltResults.Count()) }");

                #endregion

                #region Echo results

                var echoDirectories = Directory.GetDirectories(folderMap["Echo Results"].Key, folderMap["Echo Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting Echo results in { folderMap["Echo Results"].Key }");

                foreach (var echoDirectory in echoDirectories)
                {
                    foreach (var _file in GetFiles(echoDirectory, resultFilePattern))
                    {
                        echoResults.Add(_file);
                    }
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Echo results: { string.Format("{0:n0}", echoResults.Count()) }");

                #endregion

                #region Holter results

                var holterDirectories = Directory.GetDirectories(folderMap["Holter Results"].Key, folderMap["Holter Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting Holter results in { folderMap["Holter Results"].Key }");

                foreach (var holterDirectory in holterDirectories)
                {
                    foreach (var _file in GetFiles(holterDirectory, resultFilePattern))
                    {
                        holterResults.Add(_file);
                    }   
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Holter results: { string.Format("{0:n0}", holterResults.Count()) }");

                #endregion

                #region ABPM results

                var ABPMDirectories = Directory.GetDirectories(folderMap["ABPM Results"].Key, folderMap["ABPM Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting ABPM results in { folderMap["Holter Results"].Key }");

                foreach (var ABPMDirectory in ABPMDirectories)
                {
                    foreach (var _file in GetFiles(ABPMDirectory, resultFilePattern))
                    {
                        ABPMResults.Add(_file);
                    }
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] ABPM results: { string.Format("{0:n0}", ABPMResults.Count()) }");

                #endregion

                #region Stress Test results

                var stressTestDirectories = Directory.GetDirectories(folderMap["Stress Test Results"].Key, folderMap["Stress Test Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting Stress Test results in { folderMap["Stress Test Results"].Key }");

                foreach (var stressTestDirectory in stressTestDirectories)
                {
                    foreach (var _file in GetFiles(stressTestDirectory, resultFilePattern))
                    {
                        stressTestResults.Add(_file);
                    }
                }

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Stress Test results: { string.Format("{0:n0}", stressTestResults.Count()) }");

                #endregion

                #region ECG reults

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Getting ECG results in { folderMap["ECG Results"].Key }");
                var ECGResults = GetFiles(folderMap["ECG Results"].Key, folderMap["ECG Results"].Value);
                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] ECG results: { string.Format("{0:n0}", ECGResults.Count()) }");

                #endregion

                Console.WriteLine($"[CVL Emailing: Initialize Process] > [Initialize Transactions] Total results gathered: " + 
                                  $"{ string.Format("{0:n0}", vascularResults.Count() + tiltResults.Count() + echoResults.Count() + holterResults.Count() + ABPMResults.Count() + stressTestResults.Count() + ECGResults.Count()) }");

                #endregion

                #region Process data

                return new Results(vascularResults.ToArray(), tiltResults.ToArray(), echoResults.ToArray(), holterResults.ToArray(), ABPMResults.ToArray(), stressTestResults.ToArray(), ECGResults.ToArray());

                #endregion
            }
            catch (Exception exception)
            {
                throw new Exception("GetResults error", exception);
            }
        }

        #region Helper members

        private static string resultFilePattern = @"*_*.pdf";

        private static string[] GetFiles(string sourceFolder, string pattern)
        {
            try
            {
                #region Initialize variables

                #endregion

                #region Gather data

                #endregion

                #region Process data

                var files = from _file in new DirectoryInfo(sourceFolder).GetFiles(pattern, SearchOption.AllDirectories)
                            where !_file.Attributes.HasFlag(FileAttributes.Hidden)
                            select _file.FullName.Trim();

                #endregion

                return files.ToArray();
            }
            catch (Exception exception)
            {
                throw new Exception($"GetFiles error - { sourceFolder }", exception);
            }
        }

        #endregion
    }
}