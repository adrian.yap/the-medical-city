﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;

namespace TMC.CVLEmailing.Processes
{
    /// <summary>
    /// 
    /// Custom class with methods for reporting.
    /// 
    /// Used in the following activities:
    /// - Reporting
    /// 
    /// </summary>

    public class Reporting
    {
        /// <summary>
        /// Add entry.
        /// </summary>
        /// <param name="reportType"></param>
        /// <param name="DBFile"></param>
        /// <param name="patientDetails"></param>
        /// <param name="procedure"></param>
        public static void AddEntry(ReportType reportType, string DBFile, PatientDetails patientDetails, Procedure procedure)
        {
            try
            {
                var SQLiteCommand = string.Empty;

                switch (reportType)
                {
                    case ReportType.Pending:
                        SQLiteCommand = "SELECT rowid, * FROM Pending;";
                        var pending = Database.AccessDatabase(DBFile, SQLiteCommand);

                        if (HasNoRecord(pending, patientDetails, procedure))
                        {
                            SQLiteCommand = $"INSERT INTO Pending ( pin_number, patient_name, procedure_name, procedure_date, date_and_time_sent ) " +
                                            $"VALUES ( '{ patientDetails.PIN }', '{ patientDetails.FullName }', '{ procedure.ProcedureName }', '{ procedure.ProcedureDate }', '{ DateTime.Now }' );";
                            Database.AccessDatabase(DBFile, SQLiteCommand);
                        }

                        break;
                    case ReportType.NotSent:
                        SQLiteCommand = "SELECT rowid, * FROM NotSent;";
                        var notSent = Database.AccessDatabase(DBFile, SQLiteCommand);

                        if (HasNoRecord(notSent, patientDetails, procedure))
                        {
                            SQLiteCommand = $"INSERT INTO NotSent ( pin_number, patient_name, procedure_name, procedure_date, date_and_time_sent ) " +
                                            $"VALUES ( '{ patientDetails.PIN }', '{ patientDetails.FullName }', '{ procedure.ProcedureName }', '{ procedure.ProcedureDate }', '{ DateTime.Now }' );";
                            Database.AccessDatabase(DBFile, SQLiteCommand);
                        }

                        break;
                    case ReportType.Failed:
                        SQLiteCommand = "SELECT rowid, * FROM Failed;";
                        var failed = Database.AccessDatabase(DBFile, SQLiteCommand);

                        if (HasNoRecord(failed, patientDetails, procedure))
                        {
                            SQLiteCommand = $"INSERT INTO Failed ( pin_number, patient_name, procedure_name, procedure_date, date_and_time_sent ) " +
                                            $"VALUES ( '{ patientDetails.PIN }', '{ patientDetails.FullName }', '{ procedure.ProcedureName }', '{ procedure.ProcedureDate }', '{ DateTime.Now }' );";
                            Database.AccessDatabase(DBFile, SQLiteCommand);
                        }

                        break;
                    case ReportType.Sent:
                        SQLiteCommand = "SELECT rowid, * FROM Sent;";
                        var sent = Database.AccessDatabase(DBFile, SQLiteCommand);

                        if (HasNoRecord(sent, patientDetails, procedure))
                        {
                            SQLiteCommand = $"INSERT INTO Sent ( pin_number, patient_name, procedure_name, procedure_date, date_and_time_sent ) " +
                                            $"VALUES ( '{ patientDetails.PIN }', '{ patientDetails.FullName }', '{ procedure.ProcedureName }', '{ procedure.ProcedureDate }', '{ DateTime.Now }' );";
                            Database.AccessDatabase(DBFile, SQLiteCommand);
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                throw new Exception("AddEntry error", exception);
            }
        }

        /// <summary>
        /// Get email status data.
        /// </summary>
        /// <param name="DBFile"></param>
        /// <returns></returns>
        public static Dictionary<string, DataTable> GetEmailStatusData(string DBFile)
        {
            try
            {
                #region Initialize variables

                var data = new Dictionary<string, DataTable>();
                var SQLiteCommand = string.Empty;
                var filterCommand = @"DELETE FROM {Table} AS T " +
                                    @"WHERE EXISTS (SELECT * FROM Sent AS S " +
                                                  @"WHERE T.pin_number = S.pin_number AND " +
                                                        @"T.patient_name = S.patient_name AND " +
                                                        @"T.procedure_name = S.procedure_name AND " +
                                                        @"T.procedure_date = S.procedure_date);" +
                                    @"VACUUM;" +
                                    @"SELECT* FROM {Table};";

                #endregion

                #region Gather data

                SQLiteCommand = filterCommand.Replace("{Table}", "Pending");
                var pending = Database.AccessDatabase(DBFile, SQLiteCommand);
                RenameColumns(pending);

                if (pending.Rows.Count == 0)
                {
                    pending.Rows.Add();
                }

                SQLiteCommand = filterCommand.Replace("{Table}", "Failed");
                var failed = Database.AccessDatabase(DBFile, SQLiteCommand);
                RenameColumns(failed);

                if (failed.Rows.Count == 0)
                {
                    failed.Rows.Add();
                }

                SQLiteCommand = filterCommand.Replace("{Table}", "NotSent");
                var notSent = Database.AccessDatabase(DBFile, SQLiteCommand);
                RenameColumns(notSent);

                if (notSent.Rows.Count == 0)
                {
                    notSent.Rows.Add();
                }

                SQLiteCommand = "SELECT * FROM Sent;";
                var sent = Database.AccessDatabase(DBFile, SQLiteCommand);
                RenameColumns(sent);
                
                if (sent.Rows.Count == 0)
                {
                    sent.Rows.Add();
                }

                #endregion

                #region Process data

                data.Clear();

                if (!data.ContainsKey("Pending")) data.Add("Pending", pending);
                else data["Pending"] = pending;

                if (!data.ContainsKey("Failed")) data.Add("Failed", failed);
                else data["Failed"] = failed;

                if (!data.ContainsKey("Not Sent")) data.Add("Not Sent", notSent);
                else data["Not Sent"] = notSent;

                if (!data.ContainsKey("Sent")) data.Add("Sent", sent);
                else data["Sent"] = sent;

                return data;

                #endregion
            }
            catch (Exception exception)
            {
                throw new Exception("GetEmailStatusData error", exception);
            }
        }

        #region Helper members

        private static bool HasNoRecord(DataTable data, PatientDetails patientDetails, Procedure procedure)
        {
            try
            {
                return data.AsEnumerable().Where(_row => _row["pin_number"].ToString().Trim() == patientDetails.PIN &&
                                                         _row["patient_name"].ToString().Trim() == patientDetails.FullName &&
                                                         _row["procedure_name"].ToString().Trim() == procedure.ProcedureName &&
                                                         _row["procedure_date"].ToString().Trim() == procedure.ProcedureDate).Count() == 0;
            }
            catch (Exception exception)
            {
                throw new Exception("HasNoRecord error", exception);
            }
        }

        private static void RenameColumns(DataTable data)
        {
            try
            {
                data.Columns["pin_number"].ColumnName = "PIN Number";
                data.Columns["patient_name"].ColumnName = "Patient Name";
                data.Columns["procedure_name"].ColumnName = "Procedure Name";
                data.Columns["procedure_date"].ColumnName = "Procedure Date";
                data.Columns["date_and_time_sent"].ColumnName = "Date And Time Sent";
            }
            catch (Exception exception)
            {
                throw new Exception("RenameColumns error", exception);
            }
        }

        #endregion
    }
}