﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;

namespace TMC.CVLEmailing.Processes
{
    /// <summary>
    /// 
    /// Custom class with methods for getting attachments.
    /// 
    /// Used in the following activities:
    /// - Get Attachments
    /// 
    /// </summary>
    public class AttachmentsGathering
    {
        /// <summary>
        /// Get attachments.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="procedureGroup"></param>
        /// <param name="procedure"></param>
        /// <param name="patientDetails"></param>
        /// <param name="DBFile"></param>
        /// <returns></returns>
        public static string[] GetAttachments(Results results, ProcedureGroup procedureGroup, Procedure procedure, PatientDetails patientDetails, string DBFile) 
        {
            try
            {
                #region Initialize variables

                var resultsFound = new List<string>();
                var attachments = new List<string>();

                #endregion

                #region Gather data

                #endregion

                #region Process data
                
                var SQLiteCommand = "SELECT rowid, * FROM Sent;";
                var sent = Database.AccessDatabase(DBFile, SQLiteCommand);

                switch (procedureGroup)
                {
                    case ProcedureGroup.VASCULAR:
                        resultsFound = (from _result in results.VascularResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.TILT:
                        resultsFound = (from _result in results.TiltResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.ECHO:
                        resultsFound = (from _result in results.EchoResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.HOLTER:
                        resultsFound = (from _result in results.HolterResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.ABPM:
                        resultsFound = (from _result in results.ABPMResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.STRESS:
                        resultsFound = (from _result in results.StressTestResults
                                        where ContainsProcedureCode(_result, procedure.ProcedureCode) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              ContainsLastName(_result, patientDetails.LastName) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    case ProcedureGroup.ECG:
                        resultsFound = (from _result in results.ECGResults
                                        where Path.GetFileName(_result).Trim().ToUpper().Contains(patientDetails.PIN.Trim().ToUpper()) &&
                                              ContainsOrderNumber(_result, patientDetails.OrderNumber) &&
                                              HasNoRecord(sent, patientDetails, procedure)
                                        select _result).ToList();
                        break;
                    default:
                        break;
                }

                if (resultsFound.Count() > 0)
                {
                    foreach (var _result in resultsFound)
                    {
                        attachments.Add(_result);
                    }
                }

                #endregion

                return attachments.ToArray();
            }
            catch (Exception exception)
            {
                throw new Exception($"GetAttachments error - { procedureGroup }", exception);
            }
        }

        #region Helper members

        private static bool HasNoRecord(DataTable data, PatientDetails patientDetails, Procedure procedure)
        {
            try
            {
                return data.AsEnumerable().Where(_row => _row["pin_number"].ToString().Trim() == patientDetails.PIN &&
                                                         _row["patient_name"].ToString().Trim() == patientDetails.FullName &&
                                                         _row["procedure_name"].ToString().Trim() == procedure.ProcedureName &&
                                                         _row["procedure_date"].ToString().Trim() == procedure.ProcedureDate).Count() == 0;
            }
            catch (Exception exception)
            {
                throw new Exception("HasNoRecord error", exception);
            }
        }

        private static bool ContainsProcedureCode(string result, string procedureCode)
        {
            try
            {
                return Path.GetFileName(result).Trim().ToUpper().Contains(procedureCode.Trim().ToUpper());

            }
            catch (Exception exception)
            {
                throw new Exception("ContainsProcedureCode error", exception);
            }
        }

        private static bool ContainsOrderNumber(string result, string orderNumber)
        {
            try
            {
                return Path.GetFileName(result).Trim().ToUpper().Contains(orderNumber.Trim().ToUpper());

            }
            catch (Exception exception)
            {
                throw new Exception("ContainsOrderNumber error", exception);
            }
        }

        private static bool ContainsLastName(string result, string lastName)
        {
            try
            {
                return Regex.Split(lastName, @"\s").All(_name => Path.GetFileName(result).Trim().ToUpper().Contains(_name.Trim().ToUpper()));

            }
            catch (Exception exception)
            {
                throw new Exception("ContainsLastName error", exception);
            }
        }

        #endregion
    }
}