﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using TMC.CVLEmailing.Enums;
using TMC.CVLEmailing.Models;

namespace TMC.CVLEmailing.Processes
{
    /// <summary>
    /// 
    /// Custom class with methods for gathering patient details.
    /// 
    /// Used in the following activities:
    /// - Get Patient Details
    /// 
    /// </summary>

    public class PatientDetailsGathering
    {
        /// <summary>
        /// Get SOA Number
        /// </summary>
        /// <param name="SOAStr"></param>
        /// <returns></returns>
        public static string GetSOANumber(string SOAStr)
        {
            try
            {
                #region Initialize variables

                #endregion

                #region Gather data

                #endregion

                #region Process data

                var SOANumber = Regex.Match(SOAStr, SOANumberPattern).Value.Trim();

                #endregion

                return SOANumber;
            }
            catch (Exception exception)
            {
                throw new Exception("GetSOANumber error", exception);
            }
        }

        /// <summary>
        /// Get PIN
        /// </summary>
        /// <param name="SOAStr"></param>
        /// <returns></returns>
        public static string GetPIN(string SOAStr)
        {
            try
            {
                #region Initialize variables

                #endregion

                #region Gather data

                #endregion

                #region Process data

                var PIN = Regex.Match(SOAStr, PINPattern).Value.Trim();

                #endregion

                return PIN;
            }
            catch (Exception exception)
            {
                throw new Exception("GetPIN error", exception);
            }
        }

        /// <summary>
        /// Get patient details.
        /// </summary>
        /// <param name="SOAStr"></param>
        /// <param name="datatablesDictionary"></param>
        /// <param name="loggingOption"></param>
        /// <returns></returns>
        public static PatientDetails GetPatientDetails(string SOAStr, Dictionary<string, DataTable> datatablesDictionary, LoggingOption loggingOption)
        {
            try
            {
                #region Initialize variables

                var orderNumberQuery = datatablesDictionary["Order Number Query"].AsEnumerable();
                var patientNameQuery = datatablesDictionary["Patient Name Query"].AsEnumerable();

                #endregion

                #region Gather data

                var SOANumber = GetSOANumber(SOAStr);
                var orderNumber = orderNumberQuery.Select(_row => _row["OrderNumber"].ToString().Trim()).FirstOrDefault();
                var PIN = GetPIN(SOAStr);
                var firstName = patientNameQuery.Select(_row => _row["first_name_l"].ToString().Trim()).FirstOrDefault();
                var middleName = patientNameQuery.Select(_row => _row["middle_name_l"].ToString().Trim()).FirstOrDefault();
                var lastName = patientNameQuery.Select(_row => _row["last_name_l"].ToString().Trim()).FirstOrDefault();
                var suffix = patientNameQuery.Select(_row => _row["suffix_l"].ToString().Trim()).FirstOrDefault();
                var dateOfBirth = patientNameQuery.Select(_row => _row["date_of_birth"].ToString().Trim()).FirstOrDefault();
                var procedures = GetProcedures(SOAStr, datatablesDictionary);

                #endregion

                #region Process data

                var patientDetails = new PatientDetails(SOANumber, orderNumber, PIN, firstName, middleName, lastName, suffix, dateOfBirth, procedures);
                switch (loggingOption)
                {
                    case LoggingOption.EnableLogging:
                        LogPatientDetails(patientDetails);
                        break;
                    case LoggingOption.DisableLogging:
                        break;
                    default:
                        LogPatientDetails(patientDetails);
                        break;
                }

                #endregion

                return patientDetails;
            }
            catch (Exception exception)
            {
                throw new Exception("GetPatientDetails error", exception);
            }
        }

        #region Helper members

        private static readonly string SOANumberPattern = @"[A-Z]{3}\-\d{10}";
        private static readonly string PINPattern = @"(?<=HN:)(.*)(?=Admission Date)";
        private static readonly string itemCodePattern = @"[a-zA-Z]{3}[0-9]{10}";
        private static readonly string datePattern = @"\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}\:\d{1,2}\:\d{1,2}\s[AP][M]";

        private static Procedure[] GetProcedures(string SOAStr, Dictionary<string, DataTable> datatablesDictionary)
        {
            try
            {
                #region Initialize variables

                var procedures = new List<Procedure>();
                var SOAStrLine = SOAStr.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                var itemCodeMap = datatablesDictionary["Item Code Map"].AsEnumerable();
                var CVLProcedureMap = datatablesDictionary["CVL Procedure Map"].AsEnumerable();
                var procedureCodeMap = datatablesDictionary["Procedure Code Map"].AsEnumerable();
                string itemCode, date, procedureName, procedureGroup, procedureCode;

                #endregion

                #region Gather data

                var linesWithItemCode = SOAStrLine.Where(_line => Regex.IsMatch(_line, itemCodePattern));
                var itemCodes = itemCodeMap.Select(_row => _row["item_code"].ToString().Trim()).ToArray();

                #endregion

                #region Process data

                // Check for CVL procedures
                foreach (var lineWithItemCode in linesWithItemCode)
                {
                    itemCode = Regex.Match(lineWithItemCode, itemCodePattern).Value.Trim();

                    if (itemCodes.Contains(itemCode))
                    {
                        date = Regex.Match(lineWithItemCode, datePattern).Value.Trim();

                        if (!(string.IsNullOrEmpty(date) || string.IsNullOrWhiteSpace(date)))
                        {
                            var procedureNames = from _row in itemCodeMap
                                                 where _row["item_code"].ToString().Trim() == itemCode
                                                 select _row["item_name_l"].ToString().Trim();
                            procedureName = procedureNames.Count() > 0 ? procedureNames.First() : "Procedure name not found.";

                            var procedureGroups = from _row in CVLProcedureMap
                                                  where Regex.Split(_row["Procedure Name"].ToString().Trim().ToUpper(), @"\s").
                                                        Except(Regex.Split(procedureName.ToUpper(), @"\s")).Count() == 0
                                                  select _row["Procedure Grouping"].ToString().Trim();
                            procedureGroup = procedureGroups.Count() > 0 ? procedureGroups.First() : "Procedure group not found";
                            
                            var procedureCodes = from _row in procedureCodeMap 
                                                 where Regex.Split(_row["Value"].ToString().Trim().ToUpper(), @"\s").
                                                       Except(Regex.Split(procedureName.ToUpper(), @"\s")).Count() == 0
                                                 select _row["Name"].ToString().Trim();
                            procedureCode = procedureCodes.Count() > 0 ? procedureCodes.First() : "Procedure code not found.";

                            procedures.Add(new Procedure(itemCode, date, procedureName, procedureGroup, procedureCode));
                        }
                    }
                }

                #endregion

                return procedures.ToArray();
            }
            catch (Exception exception)
            {
                throw new Exception("GetProcedures error", exception);
            }
        }

        private static void LogPatientDetails(PatientDetails patientDetails)
        {
            try
            {
                #region Initialize variables

                var lineBorder = @"==================================================";
                var procedures = new List<string>();

                #endregion

                #region Gather data

                foreach (var _procedure in patientDetails.Procedures)
                {
                    procedures.Add($"{lineBorder}\nItem Code: { _procedure.ItemCode }\nProcedure Date: { _procedure.ProcedureDate }\nProcedure Name: { _procedure.ProcedureName }\nProcedure Group: { _procedure.ProcedureGroup }\nProcedure Code: { _procedure.ProcedureCode }\n{ lineBorder }");
                }

                #endregion

                #region Process data

                Console.WriteLine($"SOA Number: { patientDetails.SOANumber }\nOrder Number: { patientDetails.OrderNumber }\nPIN: { patientDetails.PIN }\n" +
                                  $"First Name: { patientDetails.FirstName }\nMiddle Name: { patientDetails.MiddleName }\nLast Name: { patientDetails.LastName }\nSuffix: { patientDetails.Suffix }\nFull Name: { patientDetails.FullName }\nDate Of Birth: { patientDetails.DateOfBirth }\n" +
                                  $"Procedures: { patientDetails.Procedures.Count() }\n{ string.Join(Environment.NewLine, procedures) }");

                #endregion
            }
            catch (Exception exception)
            {
                throw new Exception("LogPatientDetails error", exception);
            }
        }

        #endregion
    }
}