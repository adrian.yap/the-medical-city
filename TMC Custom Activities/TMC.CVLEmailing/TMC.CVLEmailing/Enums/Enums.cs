﻿namespace TMC.CVLEmailing.Enums
{
    /// <summary>
    /// Procedure group.
    /// </summary>
    public enum ProcedureGroup
    {
        /// <summary>
        /// Vascular
        /// </summary>
        VASCULAR,

        /// <summary>
        /// Tilt
        /// </summary>
        TILT,
        
        /// <summary>
        /// Echo
        /// </summary>
        ECHO,

        /// <summary>
        /// Holter
        /// </summary>
        HOLTER,

        /// <summary>
        /// ABPM
        /// </summary>
        ABPM,

        /// <summary>
        /// Stress Test
        /// </summary>
        STRESS,

        /// <summary>
        /// ECG
        /// </summary>
        ECG
    }

    /// <summary>
    /// Logging option.
    /// </summary>
    public enum LoggingOption
    {
        /// <summary>
        /// Enable logging.
        /// </summary>
        EnableLogging,

        /// <summary>
        /// Disable logging.
        /// </summary>
        DisableLogging
    }

    /// <summary>
    /// Report type.
    /// </summary>
    public enum ReportType
    {
        /// <summary>
        /// Pending.
        /// </summary>
        Pending,

        /// <summary>
        /// Failed.
        /// </summary>
        Failed,

        /// <summary>
        /// Not sent.
        /// </summary>
        NotSent,

        /// <summary>
        /// Sent.
        /// </summary>
        Sent
    }
}